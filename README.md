# BIG BAG


[![MIT License](http://img.shields.io/badge/license-MIT-blue.svg?style=flat)](LICENSE)

ECサイトです。

## Description


ライブラリSolidusを使用しています。  
追加の商品表示機能を実装中です。


[![IMAGE](app/assets/images/img/img_bigbag.png)](https://bigbag.herokuapp.com/potepan)

## Features


- カテゴリーツリー
- 関連商品表示
- 新着商品表示
- FILTER BY COLOR
- FILTER BY SORT
- FILTER BY SIZE
- 商品一覧表示切り替え
- ページネーション
- 商品検索
- カート

## Usage

###### Web page:
- [bigbag](https://bigbag.herokuapp.com/potepan)
- [管理ページ](https://bigbag.herokuapp.com/admin)
  -  Email: bigbag@example.com
  -  Password: bgec3110

###### Local:
1. git clone
2. docker-compose up --build(Docker未インストールの方はインストールしDockerを起動してください)

## Installation

###### SSH:

    $ git clone git@bitbucket.org:7kamaru/potepanec.git

###### HTTPS:

    $ git clone https://7kamaru@bitbucket.org/7kamaru/potepanec.git

## Author


[@Nakamaru](https://twitter.com/Nakamar84486715)

## License


This software is released under the MIT License, see LICENSE.txt.
