begin
  require "awesome_print"
  Pry.config.print = proc { |output, value| output.puts value.ai }
rescue LoadError
  puts "no awesome_print :("
end

# 色設定メソッドの定義
def Pry.set_color(sym, color)
  CodeRay::Encoders::Terminal::TOKEN_COLORS[sym] = color.to_s
  { sym => color.to_s }
end

# 色設定の変更
Pry.set_color :constant, "\e[1;36m"
Pry.set_color :doctype, "\e[1;36m"
Pry.set_color :octal, "\e[1;36m"
Pry.set_color :pseudo_class, "\e[1;36m"
Pry.set_color :annotation, "\e[1;36m"
Pry.set_color :comment, "\e[1;36m"
Pry.set_color :done, "\e[1;36m"
Pry.set_color :function, "\e[1;36m"
Pry.set_color :integer, "\e[1;36m"
Pry.set_color :imaginary, "\e[1;36m"
Pry.set_color :type, "\e[1;36m"
Pry.set_color :variable, "\e[1;36m"
Pry.set_color :method, "\e[1;36m"
Pry.set_color :simbol, "\e[1;36m"
Pry.set_color :namespace, "\e[1;36m"
