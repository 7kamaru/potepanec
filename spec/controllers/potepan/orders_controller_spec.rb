require 'rails_helper'

RSpec.describe Potepan::OrdersController, type: :controller do
  let!(:store) { create(:store) }

  describe "#index" do
    let(:guest_token) { 'abcde' }
    let!(:order) do
      create(
        :order_with_line_items,
        line_items_count: 2,
        guest_token: guest_token
      )
    end
    let(:line_items) { order.line_items }

    before do
      cookies.signed[:guest_token] = guest_token
      get :index
    end

    it "responds successfully" do
      expect(response).to be_successful
    end

    it "renders index" do
      expect(response).to render_template :index
    end

    it "assigns the guest's current order to @order" do
      expect(assigns(:order)).to eq order
    end

    it "assigns the guest's current line items to @line_items" do
      expect(assigns(:line_items)).to eq line_items
    end
  end

  describe "#create" do
    context "with no order" do
      context "before create orders" do
        it "has no order" do
          expect(Spree::Order.count).to eq 0
        end

        it "has no line_item" do
          expect(Spree::LineItem.count).to eq 0
        end
      end

      context "after create a order" do
        let(:variant) { create(:variant) }

        before do
          post :create, params: { line_item: { quantity: 1 }, variant_id: variant.id }
        end

        it "has one order" do
          expect(Spree::Order.count).to eq 1
        end

        it "has one line item" do
          expect(Spree::LineItem.count).to eq 1
        end
      end
    end

    context "with one order" do
      let!(:order) { create(:order, guest_token: 'abc') }
      let!(:line_item) { create(:line_item, order: order) }

      context "before create orders" do
        it "has one order" do
          expect(Spree::Order.count).to eq 1
        end

        it "has one line_item" do
          expect(Spree::LineItem.count).to eq 1
        end
      end

      context "after create a order" do
        let(:variant) { create(:variant) }

        before do
          cookies.signed[:guest_token] = 'abc'
          post :create, params: { line_item: { quantity: 1 }, variant_id: variant.id }
          order.reload
        end

        it "has one order" do
          expect(Spree::Order.count).to eq 1
        end

        it "has two line item" do
          expect(Spree::LineItem.count).to eq 2
        end
      end
    end
  end

  describe "#update" do
    let!(:order) { create(:order) }
    let!(:line_item) do
      create(
        :line_item,
        quantity: 2,
        order: order
      )
    end

    context "with calculation: 'plus'" do
      let(:plus) { 'plus' }
      let(:order_params) do
        {
          calculation: plus,
          number: order.number,
          order: {
            line_items_attributes: [{ id: line_item.id }],
          },
          id: order.id,
        }
      end

      context "before update the line item of the order" do
        it "checks that the line item quantity is 2" do
          expect(line_item.quantity).to eq 2
        end
      end

      context "after update the line item of the order" do
        before do
          patch :update, xhr: true, params: order_params
          line_item.reload
        end

        it "checks that the line item quantity is 3" do
          expect(line_item.quantity).to eq 3
        end

        it "assigns the order to @order " do
          expect(assigns(:order)).to eq order
        end

        it "assigns the updated line_item to @line_items" do
          expect(assigns(:line_item)).to eq line_item
        end
      end
    end

    context "with calculation: 'minus'" do
      let(:minus) { 'minus' }
      let(:order_params) do
        {
          calculation: minus,
          number: order.number,
          order: {
            line_items_attributes: [{ id: line_item.id }],
          },
          id: order.id,
        }
      end

      context "before update the line item of the order" do
        it "checks that the line item quantity is 2" do
          expect(line_item.quantity).to eq 2
        end
      end

      context "after update the line item of the order" do
        before do
          patch :update, xhr: true, params: order_params
          line_item.reload
        end

        it "checks that the line item quantity is 1" do
          expect(line_item.quantity).to eq 1
        end

        it "assigns the order to @order " do
          expect(assigns(:order)).to eq order
        end

        it "assigns the updated line_item to @line_items" do
          expect(assigns(:line_item)).to eq line_item
        end
      end
    end
  end

  describe "#destroy" do
    let!(:order) { create(:order) }
    let!(:line_items) do
      [
        create(:line_item, order: order),
        create(:line_item, order: order),
      ]
    end

    context "before destroy the line item" do
      it "has only that order" do
        expect(Spree::Order.count).to eq 1
      end

      it "has 2 line item" do
        expect(Spree::LineItem.count).to eq 2
      end
    end

    context "after destory the line item" do
      before do
        delete :destroy, xhr: true, params: { id: line_items[0].id }
      end

      it "has only that order" do
        expect(Spree::Order.count).to eq 1
      end

      it "doesn't have 1 line item" do
        expect(Spree::LineItem.count).to eq 1
      end

      it "assigns the order to @order" do
        expect(assigns(:order)).to eq order
      end
    end
  end
end
