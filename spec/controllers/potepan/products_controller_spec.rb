require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#index" do
    let!(:taxonomy) { create(:taxonomy, id: 1) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:taxons) do
      [
        create(:taxon, name: 'Category1', taxonomy: taxonomy, parent: taxon),
        create(:taxon, name: 'Category2', taxonomy: taxonomy, parent: taxon),
        create(:taxon, name: 'Category3', taxonomy: taxonomy, parent: taxon),
        create(:taxon, name: 'Category4', taxonomy: taxonomy, parent: taxon),
      ]
    end
    let!(:old_product1) { create(:product, available_on: 1.days.ago, taxons: [taxons[0]]) }
    let!(:old_product2) { create(:product, available_on: 1.days.ago, taxons: [taxons[1]]) }
    let!(:old_product3) { create(:product, available_on: 1.days.ago, taxons: [taxons[2]]) }
    let!(:new_products) { create_list(:product, 8, available_on: Time.now, taxons: [taxons[3]]) }

    context "when there is no line item" do
      before { get :index }

      it "assigns 3 popular categories to @popular_categories" do
        expect(assigns(:popular_categories).size).to eq 3
      end

      it "assigns 3 display_products_in_popular_categories to @display_products_in_popular_categories" do
        expect(assigns(:display_products_in_popular_categories).size).to eq 3
      end

      it "assigns the 8 new_products to @new_products" do
        expect(assigns(:new_products)).to match_array new_products
      end

      it "responds successfully" do
        expect(response).to be_successful
      end

      it "returns a 200 response" do
        expect(response).to have_http_status "200"
      end

      it "assigns empty objecty to @line_items" do
        expect(assigns(:line_items)).to eq []
      end
    end

    context "when there is the order that has some line items" do
      let!(:store) { create(:store) }
      let(:guest_token) { 'abcde' }
      let!(:order) do
        create(
          :order_with_line_items,
          line_items_count: 2,
          guest_token: guest_token
        )
      end
      let(:line_items) { order.line_items }

      before do
        cookies.signed[:guest_token] = guest_token
        get :index
      end

      it "assigns the guest's current order to @order" do
        expect(assigns(:order)).to eq order
      end

      it "assigns the guest's current line items to @line_items" do
        expect(assigns(:line_items)).to eq line_items
      end
    end
  end

  describe "#show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:taxons) do
      [
        create(:taxon, taxonomy: taxonomy, parent: taxon),
        create(:taxon, taxonomy: taxonomy, parent: taxon),
      ]
    end
    let(:product) { create(:product, taxons: [taxons[0], taxons[1]]) }
    let!(:related_products) { create_list(:product, 8, taxons: [taxons[0]]) }
    let(:images) { product.variant_images }

    context "with a valid path" do
      before { get :show, params: { id: product.id } }

      it "assigns the request product to @product" do
        expect(assigns(:product)).to eq product
      end

      it "assigns the request images to @images" do
        expect(assigns(:images)).to eq images
      end

      it "provides @related_products with up to 4 elements" do
        expect(assigns(:related_products).size).to eq 4
      end

      it "responds successfully" do
        expect(response).to be_successful
      end

      it "returns a 200 response" do
        expect(response).to have_http_status "200"
      end
    end

    context "with a Invalid path" do
      before { get :show, params: { id: 0 } }

      it "returns a 302 response" do
        expect(response).to have_http_status "302"
      end

      it "sets the flash" do
        expect(flash[:danger]).to eq "お探しの商品は見つかりませんでした。"
      end

      it "redirects to the index page" do
        expect(response).to redirect_to potepan_path
      end
    end
  end
end
