require 'rails_helper'

# Spree::Productインタンスが3つ余計に作られます。原因不明。

RSpec.describe Potepan::CategoriesController, type: :controller do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxons) do
    [
      create(:taxon, taxonomy: taxonomy, parent: taxon),
      create(:taxon, taxonomy: taxonomy, parent: taxon),
    ]
  end
  let!(:option_types) do
    [
      create(:option_type, presentation: 'Color'),
      create(:option_type, presentation: 'Size'),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: 'Red', presentation: 'Red', option_type: option_types[0]),
      create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_types[0]),
    ]
  end
  let!(:sizes) do
    [
      create(:option_value, name: 'Medium', presentation: 'M', option_type: option_types[1]),
      create(:option_value, name: 'Large', presentation: 'L', option_type: option_types[1]),
    ]
  end
  let!(:variants) do
    [
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [colors[1]]),
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [sizes[0]]),
      create(:variant, option_values: [sizes[1]]),
      create(:variant, option_values: [sizes[0]]),
    ]
  end
  let!(:products) do
    [
      create(
        :product,
        name: 'product0',
        description: search_words[1],
        price: 10.00,
        available_on: Date.current.in_time_zone,
        taxons: [taxons[0]],
        variants: [variants[0], variants[3]]
      ),
      create(
        :product,
        name: 'product1',
        description: search_words[0],
        price: 20.00,
        available_on: 1.day.ago,
        taxons: [taxons[1]],
        variants: [variants[1], variants[4]]
      ),
      create(
        :product,
        name: 'product2',
        description: search_words[1],
        price: 30.00,
        available_on: 2.years.ago,
        taxons: [taxons[0]],
        variants: [variants[2], variants[5]]
      ),
    ]
  end
  let!(:sort_values) do
    [
      ["新着順", "NEW_ARRIVAL"],
      ["価格の安い順", "LOW_PRICE"],
      ["価格の高い順", "HIGH_PRICE"],
      ["古い順", "OLD_ARRIVAL"],
    ]
  end
  let!(:search_words) do
    [
      ['Lorem ipsum dolor sit amet,'],
      ['Sed ut perspiciatis unde omnis'],
    ]
  end

  describe "#index" do
    context "when the number of products to display is 9 or less" do
      before { get :index }

      context "if there is no url query" do
        it "assigns the request taxonomy to @taxonomies" do
          expect(assigns(:taxonomies)).to match_array taxonomy
        end

        it "assigns all registered colors to @colors" do
          expect(assigns(:colors)).to match_array colors
        end

        it "assigns all registered sizes to @sizes" do
          expect(assigns(:sizes)).to match_array sizes
        end

        it "assigns all registered sort values to @sort_values" do
          expect(assigns(:sort_values)).to match_array sort_values
        end

        it "assigns default order NEW_ARRIVAL to @selected_sort_value" do
          expect(assigns(:selected_sort_value)).to eq sort_values[0][1]
        end

        it "assigns all products to @products" do
          expect(assigns(:products).size).to eq Spree::Product.pluck(:id).size
        end

        it "assigns ids of all products to @ids_of_products_in_current_taxon" do
          expect(assigns(:ids_of_products_in_current_taxon)).to eq Spree::Product.pluck(:id)
        end

        it "responds successfully" do
          expect(response).to be_successful
        end
      end

      context "if there is params[:search_word]" do
        let!(:variants_for_search) do
          [
            create(:variant, option_values: [colors[1]]),
            create(:variant, option_values: [sizes[1]]),
          ]
        end
        let!(:product_for_search) do
          create(
            :product,
            name: 'product_for_search',
            description: search_words[1],
            price: 20.00,
            available_on: 1.day.ago,
            taxons: [taxons[0]],
            variants: [variants_for_search[0], variants_for_search[1]]
          )
        end

        before do
          get :index, params: { search_word: search_words[1] }
        end

        it "assigns products caught in search to @products" do
          expect(assigns(:products)).to match_array [products[0], products[2], product_for_search]
        end

        context "and params[:sort_order] and params[:color_name]" do
          context "in non-default order" do
            it "assigns products caught in search and that match the selected color in the selected order to @products" do
              get :index, params: { sort_order: sort_values[2][1], color_name: colors[0].name, search_word: search_words[1] }
              expect(assigns(:products)).to match_array [products[0], products[2]]
              expect(assigns(:products).first).to eq products[2]
            end
          end
        end

        context "and params[:sort_order] and params[:size_name]" do
          context "in non-default order" do
            it "assigns products caught in search and that match the selected size in the selected order to @products" do
              get :index, params: { sort_order: sort_values[2][1], size_name: sizes[0].name, search_word: search_words[1] }
              expect(assigns(:products)).to match_array [products[0], products[2]]
              expect(assigns(:products).first).to eq products[2]
            end
          end
        end

        context "and params[:sort_order]" do
          context "that isn't default order" do
            it "assigns products caught in search in the selected order to @products" do
              get :index, params: { sort_order: sort_values[2][1], search_word: search_words[1] }
              expect(assigns(:products)).to match_array [products[0], products[2], product_for_search]
              expect(assigns(:products).first).to eq products[2]
            end
          end
        end

        context "and params[:color_name]" do
          it "assigns products caught in search and that match the selected color to @products" do
            get :index, params: { color_name: colors[0].name, search_word: search_words[1] }
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end
        end

        context "and params[:size_name]" do
          it "assigns products caught in search and that match the selected size to @products" do
            get :index, params: { size_name: sizes[0].name, search_word: search_words[1] }
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end
        end
      end

      context "if there is params[:sort_order] and params[:color_name]" do
        context "of non-default order" do
          before do
            get :index, params: { sort_order: sort_values[2][1], color_name: colors[0].name }
          end

          it "assigns products in selected order to @products" do
            expect(assigns(:products).first).to eq products[2]
          end

          it "assigns products of selected color to @products" do
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end

          it "doesn't assign products of color not selected to @products" do
            expect(assigns(:products)).not_to include products[1]
          end
        end
      end

      context "if there is params[:sort_order] and params[:size_name]" do
        context "of non-default order" do
          before do
            get :index, params: { sort_order: sort_values[2][1], size_name: sizes[0].name }
          end

          it "assigns products in selected order to @products" do
            expect(assigns(:products).first).to eq products[2]
          end

          it "assigns products of selected color to @products" do
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end

          it "doesn't assign products of color not selected to @products" do
            expect(assigns(:products)).not_to include products[1]
          end
        end
      end

      context "if there is params[:sort_order]" do
        context "of value NEW_ARRIVAL" do
          it "assigns products in the order of new arrival to @products" do
            get :index, params: { sort_order: sort_values[0][1] }
            expect(assigns(:products).first).to eq products[0]
          end
        end

        context "of value LOW_PRICE" do
          it "assigns products in the order of low price to @products" do
            get :index, params: { sort_order: sort_values[1][1] }
            expect(assigns(:products).first).to eq products[0]
          end
        end

        context "of value HIGH_PRICE" do
          it "assigns products in the order of high price to @products" do
            get :index, params: { sort_order: sort_values[2][1] }
            expect(assigns(:products).first).to eq products[2]
          end
        end

        context "of value OLD_ARRIVAL" do
          it "assigns products in the order of old arrival to @products" do
            get :index, params: { sort_order: sort_values[3][1] }
            expect(assigns(:products).first).to eq products[2]
          end
        end

        context "that isn't default order" do
          it "assigns selected order value to @selected_sort_value to @products" do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[1][1] }
            expect(assigns(:selected_sort_value)).to eq sort_values[1][1]
          end
        end
      end

      context "if there is params[:color_name]" do
        before { get :index, params: { color_name: colors[0].presentation } }

        it "assigns products of selected color to @products" do
          expect(assigns(:products)).to match_array [products[0], products[2]]
        end

        it "doesn't assign products of color not selected to @products" do
          expect(assigns(:products)).not_to include products[1]
        end
      end

      context "if there is params[:size_name]" do
        before { get :index, params: { size_name: sizes[0].name } }

        it "assigns products of selected size to @products" do
          expect(assigns(:products)).to match_array [products[0], products[2]]
        end

        it "doesn't assign products of size not selected to @products" do
          expect(assigns(:products)).not_to include products[1]
        end
      end
    end

    context "when the number of products to display is 10 or more" do
      let!(:additional_variants) do
        [
          create_list(:variant, 8, option_values: [colors[0]]),
          create_list(:variant, 8, option_values: [sizes[0]]),
        ]
      end
      let!(:additional_products) do
        [
          create(
            :product,
            variants: [additional_variants[0][0], additional_variants[1][0]]
          ),
          create(
            :product,
            variants: [additional_variants[0][1], additional_variants[1][1]]
          ),
          create(
            :product,
            variants: [additional_variants[0][2], additional_variants[1][2]]
          ),
          create(
            :product,
            variants: [additional_variants[0][3], additional_variants[1][3]]
          ),
          create(
            :product,
            variants: [additional_variants[0][4], additional_variants[1][4]]
          ),
          create(
            :product,
            variants: [additional_variants[0][5], additional_variants[1][5]]
          ),
          create(
            :product,
            variants: [additional_variants[0][6], additional_variants[1][6]]
          ),
          create(
            :product,
            variants: [additional_variants[0][7], additional_variants[1][7]]
          ),
        ]
      end

      context "if there is no url query" do
        before { get :index }

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:sort_order] and params[:color_name]" do
        before do
          get :index, params: { sort_order: sort_values[2][1], color_name: colors[0].name }
        end

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:sort_order] and params[:size_name]" do
        before do
          get :index, params: { sort_order: sort_values[2][1], size_name: sizes[0].name }
        end

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:sort_order]" do
        before do
          get :index, params: { sort_order: sort_values[2][1] }
        end

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:color_name]" do
        before do
          get :index, params: { color_name: colors[0].name }
        end

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:size_name]" do
        before do
          get :index, params: { size_name: sizes[0].name }
        end

        it "assigns 9 products to @products" do
          expect(assigns(:products).size).to eq 9
        end
      end

      context "if there is params[:page]" do
        before { get :index, params: { page: 2 } }

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end

      context "if there is params[:sort_order] and params[:color_name] and params[:page]" do
        before do
          get :index, params: { sort_order: sort_values[2][1], color_name: colors[0].name, page: 2 }
        end

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end

      context "if there is params[:sort_order] and params[:size_name] and params[:page]" do
        before do
          get :index, params: { sort_order: sort_values[2][1], size_name: sizes[0].name, page: 2 }
        end

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end

      context "if there is params[:sort_order] and params[:page]" do
        before do
          get :index, params: { sort_order: sort_values[2][1], page: 2 }
        end

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end

      context "if there is params[:color_name] and params[:page]" do
        before do
          get :index, params: { color_name: colors[0].name, page: 2 }
        end

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end

      context "if there is params[:size_name] and params[:page]" do
        before do
          get :index, params: { size_name: sizes[0].name, page: 2 }
        end

        it "assigns 9 or less products to @products" do
          expect(assigns(:products).size).to be <= 9
        end
      end
    end
  end

  describe "#show" do
    let!(:variants_in_show) do
      [
        create(:variant, option_values: [colors[1]]),
        create(:variant, option_values: [sizes[1]]),
      ]
    end
    let!(:product_in_show) do
      create(
        :product,
        taxons: [taxons[0]],
        price: 40.00,
        available_on: 3.years.ago,
        variants: [variants_in_show[0], variants_in_show[1]]
      )
    end

    context "with a valid path" do
      context "when the number of products to display is 9 or less" do
        before { get :show, params: { id: taxons[0].id } }

        context "if there is no url query" do
          it "assigns the request taxonomy to @taxonomies" do
            expect(assigns(:taxonomies)).to match_array(taxonomy)
          end

          it "assigns the request taxon to @taxon" do
            expect(assigns(:taxon)).to eq taxons[0]
          end

          it "assigns all registered colors to @colors" do
            expect(assigns(:colors)).to match_array colors
          end

          it "assigns all registered sizes to @sizes" do
            expect(assigns(:sizes)).to match_array sizes
          end

          it "assigns all registered sort values to @sort_values" do
            expect(assigns(:sort_values)).to match_array sort_values
          end

          it "assigns default order NEW_ARRIVAL to @selected_sort_value" do
            expect(assigns(:selected_sort_value)).to eq sort_values[0][1]
          end

          it "assigns all products in current taxon to @products" do
            expect(assigns(:products)).to match_array [products[0], products[2], product_in_show]
          end

          it "assigns ids of products in current taxon to @ids_of_products_in_current_taxon" do
            expect(assigns(:ids_of_products_in_current_taxon)).to eq [products[0], products[2], product_in_show].pluck(:id)
          end

          it "responds successfully" do
            expect(response).to be_successful
          end
        end

        context "if there is params[:sort_order] and params[:color_name]" do
          context "of non-default order" do
            before do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], color_name: colors[0].name }
            end

            it "assigns products in selected order to @products" do
              expect(assigns(:products).first).to eq products[2]
            end

            it "assigns products of selected color to @products" do
              expect(assigns(:products)).to match_array [products[0], products[2]]
            end

            it "doesn't assign products that are not current taxon not to @products" do
              expect(assigns(:products)).not_to include products[1]
            end

            it "doesn't assign products of color not selected to @products" do
              expect(assigns(:products)).not_to include product_in_show
            end
          end
        end

        context "if there is params[:sort_order] and params[:size_name]" do
          context "of non-default order" do
            before do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], size_name: sizes[0].name }
            end

            it "assigns products in selected order to @products" do
              expect(assigns(:products).first).to eq products[2]
            end

            it "assigns products of selected size to @products" do
              expect(assigns(:products)).to match_array [products[0], products[2]]
            end

            it "doesn't assign products that are not current taxon not to @products" do
              expect(assigns(:products)).not_to include products[1]
            end

            it "doesn't assign products of size not selected to @products" do
              expect(assigns(:products)).not_to include product_in_show
            end
          end
        end

        context "if there is params[:sort_order]" do
          context "of value NEW_ARRIVAL" do
            it "assigns products in the order of new arrival to @products" do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[0][1] }
              expect(assigns(:products).first).to eq products[0]
            end
          end

          context "of value LOW_PRICE" do
            it "assigns products in the order of low price to @products" do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[1][1] }
              expect(assigns(:products).first).to eq products[0]
            end
          end

          context "of value HIGH_PRICE" do
            it "assigns products in the order of high price to @products" do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1] }
              expect(assigns(:products).first).to eq product_in_show
            end
          end

          context "of value OLD_ARRIVAL" do
            it "assigns products in the order of old arrival to @products" do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[3][1] }
              expect(assigns(:products).first).to eq product_in_show
            end
          end

          context "that isn't default order" do
            it "assigns selected order value to @selected_sort_value to @products" do
              get :show, params: { id: taxons[0].id, sort_order: sort_values[1][1] }
              expect(assigns(:selected_sort_value)).to eq sort_values[1][1]
            end
          end
        end

        context "if there is params[:color_name]" do
          before { get :show, params: { id: taxons[0].id, color_name: colors[0].presentation } }

          it "assigns products of selected color in current taxon to @products" do
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end

          it "doesn't assign products of taxons not selected to @products" do
            expect(assigns(:products)).not_to include products[1]
          end

          it "doesn't assign products of color not selected to @products" do
            expect(assigns(:products)).not_to include product_in_show
          end
        end

        context "if there is params[:size_name]" do
          before { get :show, params: { id: taxons[0].id, size_name: sizes[0].name } }

          it "assigns products of selected size to @products" do
            expect(assigns(:products)).to match_array [products[0], products[2]]
          end

          it "doesn't assign products of taxons not selected to @products" do
            expect(assigns(:products)).not_to include products[1]
          end

          it "doesn't assign products of size not selected to @products" do
            expect(assigns(:products)).not_to include product_in_show
          end
        end
      end

      context "when the number of products to display is 10 or more" do
        let!(:additional_variants) do
          [
            create_list(:variant, 8, option_values: [colors[0]]),
            create_list(:variant, 8, option_values: [sizes[0]]),
          ]
        end
        let!(:additional_products) do
          [
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][0], additional_variants[1][0]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][1], additional_variants[1][1]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][2], additional_variants[1][2]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][3], additional_variants[1][3]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][4], additional_variants[1][4]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][5], additional_variants[1][5]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][6], additional_variants[1][6]]
            ),
            create(
              :product,
              taxons: [taxons[0]],
              variants: [additional_variants[0][7], additional_variants[1][7]]
            ),
          ]
        end

        context "if there is no url query" do
          before { get :show, params: { id: taxons[0].id } }

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:sort_order] and params[:color_name]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], color_name: colors[0].name }
          end

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:sort_order] and params[:size_name]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], size_name: sizes[0].name }
          end

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:sort_order]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1] }
          end

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:color_name]" do
          before do
            get :show, params: { id: taxons[0].id, color_name: colors[0].name }
          end

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:size_name]" do
          before do
            get :show, params: { id: taxons[0].id, size_name: sizes[0].name }
          end

          it "assigns 9 products to @products" do
            expect(assigns(:products).size).to eq 9
          end
        end

        context "if there is params[:page]" do
          before { get :show, params: { id: taxons[0].id, page: 2 } }

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end

        context "if there is params[:sort_order] and params[:color_name] and params[:page]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], color_name: colors[0].name, page: 2 }
          end

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end

        context "if there is params[:sort_order] and params[:size_name] and params[:page]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], size_name: sizes[0].name, page: 2 }
          end

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end

        context "if there is params[:sort_order] and params[:page]" do
          before do
            get :show, params: { id: taxons[0].id, sort_order: sort_values[2][1], page: 2 }
          end

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end

        context "if there is params[:color_name] and params[:page]" do
          before do
            get :show, params: { id: taxons[0].id, color_name: colors[0].name, page: 2 }
          end

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end

        context "if there is params[:size_name] and params[:page]" do
          before do
            get :show, params: { id: taxons[0].id, size_name: sizes[0].name, page: 2 }
          end

          it "assigns 9 or less products to @products" do
            expect(assigns(:products).size).to be <= 9
          end
        end
      end
    end

    context "with a invalid path" do
      before { get :show, params: { id: 0 } }

      it "returns a 302 response" do
        expect(response).to have_http_status "302"
      end

      it "sets the flash" do
        expect(flash[:danger]).to eq "お探しの商品は見つかりませんでした。"
      end

      it "redirects to the index page" do
        expect(response).to redirect_to potepan_path
      end
    end
  end

  describe "#grid" do
    before { get :grid, xhr: true }

    it "assigns nil to session[:display_style]" do
      expect(session[:display_style]).to eq nil
    end
  end

  describe "#list" do
    before { get :list, xhr: true }

    it "assigns 'list' to session[:display_style]" do
      expect(session[:display_style]).to eq 'list'
    end
  end
end
