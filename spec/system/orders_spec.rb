require 'rails_helper'

# 商品のupdate, destroyはテストしておりません。
# chromeヘッドレスドライバ導入不可のため。

RSpec.describe "Orders", type: :system do
  let!(:store) { create(:store) }
  let!(:taxon) { create(:taxon) }
  let!(:product) { create(:product, taxons: [taxon], price: 2000) }

  context "When adding two products to the cart" do
    context "from potepan_product_path" do
      before do
        visit potepan_product_path(product.id)
        select "2", from: "quantity"
        click_on "カートに入れる"
      end

      context "moves to potepan_orders_path, where" do
        let(:order) { Spree::Order.last }
        let(:line_item) { order.line_items.first }
        let(:consumption_tax_rate) { 0.08 }
        let(:shipping_cost_per_1) { 200 }
        let(:shipping_cost) { 650 }
        let(:item_or_items) { order.item_or_items }
        let(:consumption_tax_total) do
          order.consumption_tax_total(consumption_tax_rate: consumption_tax_rate)
        end
        let(:shipping_cost_total) do
          order.shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)
        end
        let(:order_price_total) do
          order.order_price_total(consumption_tax_total: consumption_tax_total, shipment_cost_total: shipping_cost_total)
        end

        it "checks that the current url is potepan_orders_url" do
          expect(current_url).to eq potepan_orders_url
        end

        context "in '.dropdownCart'" do
          it "displays the number of items in the order" do
            within('.cart-heading-1') do
              expect(page).to have_content order.item_count
            end
          end

          it "displays the total price of the order" do
            within('.cart-heading-2') do
              expect(page).to have_content "#{order_price_total}円"
            end
          end

          it "displays a title of line items dropdown list as 'items' in your cart" do
            within('.title-line-items') do
              expect(page).to have_content "items in your cart"
            end
          end

          it "displays a link to delete the line item" do
            within('.media-span-delete') do
              expect(page).to have_link "×", href: potepan_order_path(id: line_item.id)
            end
          end

          it "displays the product image that links to potepan_product_path(id: product.id)" do
            within('.media-box') do
              expect(page).to have_link 'cart-Image', href: potepan_product_path(id: product.id)
            end
          end

          it "displays a minus update button" do
            within('.quantity-minus-dropdown') do
              expect(page).to have_link "-", href: potepan_order_path(order: { line_items_attributes: [id: line_item.id] }, number: order.number, id: order.id, calculation: 'minus')
            end
          end

          it "displays the line item quantity" do
            within('.quantity-display-dropdown') do
              expect(page).to have_content line_item.quantity
            end
          end

          it "displays a plus update button" do
            within('.quantity-plus-dropdown') do
              expect(page).to have_link "+", href: potepan_order_path(order: { line_items_attributes: [id: line_item.id] }, number: order.number, id: order.id, calculation: 'plus')
            end
          end

          it "displays the product name that links to potepan_product_path(id: product.id)" do
            within('.media-heading') do
              expect(page).to have_link product.name, href: potepan_product_path(id: product.id)
            end
          end

          it "displays display price of the product" do
            within('.media-heading-price') do
              expect(page).to have_content product.display_price
            end
          end
        end

        context "in '.table-responsive'" do
          it "displays a link to delete the line item" do
            within('.table-responsive') do
              expect(page).to have_link "×", href: potepan_order_path(id: line_item.id)
            end
          end

          it "displays the product image that links to potepan_product_path(id: product.id)" do
            within('.table-responsive') do
              expect(page).to have_link 'cart-image', href: potepan_product_path(id: product.id)
            end
          end

          it "displays the product name that links to potepan_product_path(id: product.id)" do
            within('.table-responsive') do
              expect(page).to have_link product.name, href: potepan_product_path(id: product.id)
            end
          end

          it "displays a minus update button" do
            within('.quantity-minus') do
              expect(page).to have_link "-", href: potepan_order_path(order: { line_items_attributes: [id: line_item.id] }, number: order.number, id: order.id, calculation: 'minus')
            end
          end

          it "displays the line item quantity" do
            within('.quantity-display') do
              expect(page).to have_content line_item.quantity
            end
          end

          it "displays a plus update button" do
            within('.quantity-plus') do
              expect(page).to have_link "+", href: potepan_order_path(order: { line_items_attributes: [id: line_item.id] }, number: order.number, id: order.id, calculation: 'plus')
            end
          end

          it "displays display price of the product" do
            within('.table-responsive') do
              expect(page).to have_content product.display_price
            end
          end

          it "displays subtotal of the product" do
            within('.table-responsive') do
              expect(page).to have_content line_item.display_amount
            end
          end
        end

        context "in '.totalAmountArea'" do
          it "displays subtotal of the order" do
            within('.totalAmountArea') do
              expect(page).to have_content order.display_item_total
            end
          end

          it "displays consumption tax of the order" do
            within('.totalAmountArea') do
              expect(page).to have_content "#{consumption_tax_total}円"
            end
          end

          it "displays shipping cost of the order" do
            within('.totalAmountArea') do
              expect(page).to have_content "#{shipping_cost_total}円"
            end
          end

          it "displays the total price of the order" do
            within('.totalAmountArea') do
              expect(page).to have_content "#{order_price_total}円"
            end
          end
        end
      end
    end
  end

  context "When adding a product to the cart" do
    context "from potepan_product_path" do
      before do
        visit potepan_product_path(product.id)
        select "1", from: "quantity"
        click_on "カートに入れる"
      end

      context "moves to potepan_orders_path, where" do
        let(:order) { Spree::Order.last }
        let(:line_item) { order.line_items.first }

        it "checks that the current url is potepan_orders_url" do
          expect(current_url).to eq potepan_orders_url
        end

        context "in '.dropdownCart'" do
          it "displays a title of the line items dropdown list as 'item' in your cart" do
            within('.title-line-items') do
              expect(page).to have_content "item in your cart"
            end
          end

          it "displays a minus update button that plays the role of deletion" do
            within('.quantity-minus-dropdown') do
              expect(page).to have_link "-", href: potepan_order_path(id: line_item.id)
            end
          end
        end

        context "in '.table-responsive'" do
          it "displays a minus update button that plays the role of deletion" do
            within('.quantity-minus') do
              expect(page).to have_link "-", href: potepan_order_path(id: line_item.id)
            end
          end
        end
      end
    end
  end
end
