require 'rails_helper'

RSpec.describe "Products", type: :system do
  let(:now) { Time.current }
  let!(:taxonomy) { create(:taxonomy, id: 1) }
  let!(:taxon1) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxon2) { create(:taxon, name: 'Category1', taxonomy: taxonomy, parent: taxon1) }
  let!(:taxon3) { create(:taxon, name: 'Category2', taxonomy: taxonomy, parent: taxon1) }
  let!(:taxon4) { create(:taxon, name: 'Category3', taxonomy: taxonomy, parent: taxon1) }
  let!(:taxon5) { create(:taxon, name: 'Category4', taxonomy: taxonomy, parent: taxon1) }
  let!(:product1) { create(:product, name: 'product1', available_on: now, taxons: [taxon2]) }
  let!(:product2) { create(:product, name: 'product2', available_on: now, taxons: [taxon3]) }
  let!(:product3_to_6) { create_list(:product, 4, name: 'product3_to_6', available_on: now, taxons: [taxon2]) }
  let!(:product7_to_8) { create_list(:product, 2, name: 'product7_to_8', available_on: now, taxons: [taxon4]) }
  let!(:product9) { create(:product, name: 'product9', available_on: 1.days.ago, taxons: [taxon4, taxon5]) }
  let!(:option_types) do
    [
      create(:option_type, presentation: 'Color'),
      create(:option_type, presentation: 'Size'),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: 'Red', presentation: 'Red', option_type: option_types[0]),
      create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_types[0]),
    ]
  end
  let!(:variants) do
    [
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [colors[1]]),
    ]
  end

  context "on potepan_path" do
    context "if there is no line item" do
      before do
        visit potepan_path
      end

      context "in '.searchBox'" do
        it "has search form and button" do
          within('.searchBox') do
            expect(page).to have_css '.search-form'
            expect(page).to have_css '.input-group-addon'
          end
        end
      end

      context "in '.dropdownCart'" do
        it "doesn't display total number of line items" do
          within('.dropdownCart') do
            expect(page).not_to have_css '.cart-heading-1'
          end
        end

        it "doesn't display total price of a order" do
          within('.dropdownCart') do
            expect(page).not_to have_css '.cart-heading-2'
          end
        end

        it "doesn't display dropdown list of line items" do
          within('.dropdownCart') do
            expect(page).not_to have_css '.dropdown-menu-right'
          end
        end
      end

      context "in '.featuredCollection'" do
        it "displays popular category infomation" do
          expect(page).to have_link(taxon2.name, href: potepan_category_path(taxon2.id)).or have_link taxon3.name, href: potepan_category_path(taxon3.id)
        end

        it "displays 3 popular categories" do
          expect(all('.imageWrapper').size).to eq 3
        end
      end

      context "in '.featuredProducts'" do
        it "displays new product infomation" do
          expect(page).to have_content product1.name
          expect(page).to have_content product1.display_price
          expect(page).to have_link 'new_product', href: potepan_product_path(product1.id)
        end

        it "displays 8 new products" do
          expect(all('.slide').size).to eq 16
        end

        it "doesn't display the 9th product in the order of arrival" do
          expect(page).not_to have_content product9.name
        end
      end
    end

    context "if there is some line items" do
      let!(:store) { create(:store) }

      before do
        visit potepan_product_path(product1.id)
        select "1", from: "quantity"
        click_on "カートに入れる"
        visit potepan_path
      end

      context "in '.dropdownCart'" do
        it "displays total number of line items" do
          within('.dropdownCart') do
            expect(page).to have_css '.cart-heading-1'
          end
        end

        it "displays total price of a order" do
          within('.dropdownCart') do
            expect(page).to have_css '.cart-heading-2'
          end
        end

        it "displays dropdown list of line items" do
          within('.dropdownCart') do
            expect(page).to have_css '.dropdown-menu-right'
          end
        end
      end
    end
  end

  context "on potepan_product_path" do
    context "with a valid path" do
      before do
        visit potepan_product_path(product1.id)
      end

      it "displays products/show" do
        expect(page).to have_title product1.name
        expect(page).to have_content product1.name
        expect(page).to have_content product1.display_price
        expect(page).to have_content product1.description
      end

      it "has search form and button" do
        within('.searchBox') do
          expect(page).to have_css '.search-form'
          expect(page).to have_css '.input-group-addon'
        end
      end

      context "In related products display" do
        it "displays related products" do
          expect(page).to have_content product3_to_6.first.name
          expect(page).to have_content product3_to_6.first.display_price
          expect(page).to have_link 'related_product', href: potepan_product_path(product3_to_6.first.id)
        end

        it "doesn't display @product" do
          within ".productsContent" do
            expect(page).not_to have_content product1.name
          end
        end

        it "doesn't display different category products" do
          expect(page).not_to have_content product2.name
        end

        it "displays up to 4 related products" do
          expect(all('.productBox').size).to eq 4
        end
      end

      context "when referrer is potepan_index_path" do
        before do
          visit potepan_path
          visit potepan_product_path(product9.id)
        end

        it "links to potepan_categories_path(product.taxons.ids.first)" do
          click_link '一覧ページへ戻る'
          expect(page).to have_title taxon4.name
        end

        it "displays HOME/SHOP/@product first taxon name link in breadcrumbs" do
          within('.pageHeader') do
            expect(page).to have_link 'HOME', href: potepan_path
            expect(page).to have_link 'SHOP', href: potepan_categories_path
            expect(page).to have_link taxon4.name, href: potepan_category_path(taxon4.id)
          end
        end
      end

      context "when referrer is potepan_category_path" do
        before do
          visit potepan_category_path(taxon5.id)
          click_link product9.name
        end

        it "links to referrer" do
          click_link '一覧ページへ戻る'
          expect(page).to have_title taxon5.name
        end

        it "displays HOME/SHOP/taxon name in referrer url link in breadcrumbs" do
          within('.pageHeader') do
            expect(page).to have_link 'HOME', href: potepan_path
            expect(page).to have_link 'SHOP', href: potepan_categories_path
            expect(page).to have_link taxon5.name, href: potepan_category_path(taxon5.id)
          end
        end
      end
    end

    context "with a invalid path" do
      it "can't displays products/show" do
        visit potepan_product_path(0)
        expect(page).to have_title "index"
        expect(page).to have_css "div.alert"
      end
    end
  end
end
