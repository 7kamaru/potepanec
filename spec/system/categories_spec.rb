require 'rails_helper'

# Spree::Productインスタンスが8つ余計に作られます。原因不明。

RSpec.describe "Categories", type: :system do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxons) do
    [
      create(:taxon, taxonomy: taxonomy, parent: taxon, name: 'Shirt'),
      create(:taxon, taxonomy: taxonomy, parent: taxon, name: 'Bag'),
    ]
  end
  let!(:option_types) do
    [
      create(:option_type, presentation: 'Color'),
      create(:option_type, presentation: 'Size'),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: 'Red', presentation: 'Red', option_type: option_types[0]),
      create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_types[0]),
    ]
  end
  let!(:sizes) do
    [
      create(:option_value, name: 'Medium', presentation: 'M', option_type: option_types[1]),
      create(:option_value, name: 'Large', presentation: 'L', option_type: option_types[1]),
    ]
  end
  let!(:variants) do
    [
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [colors[1]]),
      create(:variant, option_values: [colors[1]]),
      create(:variant, option_values: [colors[1]]),
      create(:variant, option_values: [sizes[0]]),
      create(:variant, option_values: [sizes[1]]),
      create(:variant, option_values: [sizes[1]]),
      create(:variant, option_values: [sizes[1]]),
    ]
  end
  let!(:product) { create(:product, taxons: [taxons[0]]) }
  let!(:products) do
    [
      create(
        :product,
        name: 'product0',
        description: 'Lorem ipsum dolor sit amet,',
        price: 10.00,
        available_on: 3.day.ago,
        taxons: [taxons[1]],
        variants: [variants[0], variants[4]]
      ),
      create(
        :product,
        name: 'product1',
        description: 'Sed ut perspiciatis unde omnis',
        price: 20.00,
        available_on: 2.day.ago,
        taxons: [taxons[1]],
        variants: [variants[1], variants[5]]
      ),
      create(
        :product,
        name: 'product2',
        description: 'Sed ut perspiciatis unde omnis',
        price: 30.00,
        available_on: 1.day.ago,
        taxons: [taxons[1]],
        variants: [variants[2], variants[6]]
      ),
      create(
        :product,
        name: 'product3',
        description: 'Sed ut perspiciatis unde omnis',
        price: 40.00,
        available_on: Date.current.in_time_zone,
        taxons: [taxons[1]],
        variants: [variants[3], variants[7]]
      ),
    ]
  end

  # ------ on potepan_categories_path ------
  context "on potepan_categories_path" do
    before { visit potepan_categories_path }

    it "has a title 'SHOP'" do
      expect(page).to have_title 'SHOP'
    end

    it "has search form and button" do
      within('.searchBox') do
        expect(page).to have_css '.search-form'
        expect(page).to have_css '.input-group-addon'
      end
    end

    it "displays HOME/ link in breadcrumbs" do
      within('.pageHeader') do
        expect(page).to have_link 'HOME', href: potepan_path
      end
    end

    it "has link to potepan_category_path(taxons[0].id)" do
      expect(page).to have_link taxons[0].name, href: "#{potepan_category_path(taxons[0].id)}?sort_order=NEW_ARRIVAL"
    end

    it "has a link to potepan_product_path(product.id)" do
      expect(page).to have_link products[0].display_price.to_s, href: potepan_product_path(products[0].id)
      expect(page).to have_link 'product_image', href: potepan_product_path(products[0].id)
    end

    it "has taxonomy and taxon values" do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content "#{taxons[0].name} (#{taxons[0].products.size})"
      expect(page).to have_content "#{taxons[1].name} (#{taxons[1].products.size})"
    end

    it "has links of color presentations" do
      expect(page).to have_link colors[0].presentation, href: "#{potepan_categories_path}?color_name=#{colors[0].name}&sort_order=NEW_ARRIVAL"
      expect(page).to have_link colors[1].presentation, href: "#{potepan_categories_path}?color_name=#{colors[1].name}&sort_order=NEW_ARRIVAL"
    end

    it "has number of each color" do
      within(".panelColor") do
        expect(page).to have_content "(#{variants.select { |v| v.option_values == [colors[0]] }.size})"
        expect(page).to have_content "(#{variants.select { |v| v.option_values == [colors[1]] }.size})"
      end
    end

    it "has links of size presentations" do
      expect(page).to have_link sizes[0].presentation, href: "#{potepan_categories_path}?size_name=#{sizes[0].name}&sort_order=NEW_ARRIVAL"
      expect(page).to have_link sizes[1].presentation, href: "#{potepan_categories_path}?size_name=#{sizes[1].name}&sort_order=NEW_ARRIVAL"
    end

    it "has number of each size" do
      within(".panelColor") do
        expect(page).to have_content "(#{variants.select { |v| v.option_values == [sizes[0]] }.size})"
        expect(page).to have_content "(#{variants.select { |v| v.option_values == [sizes[1]] }.size})"
      end
    end

    it "has buttons to switch product list display" do
      within(".displayStyle") do
        expect(page).to have_button 'Grid'
        expect(page).to have_button 'List'
      end
    end

    it "has 'Grid' button in active state" do
      within(all(".button_to").first) do
        expect(page).to have_css '.active'
      end
    end

    it "has 'List' button not in active state" do
      within(all(".button_to")[1]) do
        expect(page).not_to have_css '.active'
      end
    end

    it "has a product list display of 'Grid'" do
      within(".filterArea") do
        expect(page).not_to have_css '.productListSingle'
        expect(page).to have_css '.productBox'
        expect(page).not_to have_css '.media'
      end
    end

    # 'Pagination' with 10 or more products on potepan_categories_path
    context "when the number of products is 10 or more" do
      let!(:products_of_1_day_ago) do
        create_list(:product, 6, available_on: 1.day.ago)
      end

      before { visit potepan_categories_path }

      context "if pagination is in dafault state" do
        it "displays the first product" do
          within('.filterArea') do
            expect(page).to have_content products[3].name
          end
        end

        it "displays the 9th product" do
          within('.filterArea') do
            expect(page).to have_content products[1].name
          end
        end

        it "displays 9 products" do
          within('.filterArea') do
            expect(all('.productBox').size).to eq 9
          end
        end

        it "shows 'pagination'" do
          within('.filterArea') do
            expect(page).to have_css '.pagination'
          end
        end

        it "has a 'pagination 1' that is active" do
          within('.filterArea') do
            within(all('.active')[1]) do
              expect(page).to have_content '1'
            end
          end
        end
      end

      context "if pagination 2 is selected" do
        before do
          within('.pagination') do
            click_link '2'
          end
        end

        it "displays products after the 9th" do
          within('.filterArea') do
            expect(page).to have_content products[0].name
          end
        end

        it "displays 9 products or less" do
          within('.filterArea') do
            expect(all('.productBox').size).to be <= 9
          end
        end

        it "shows 'pagination'" do
          within('.filterArea') do
            expect(page).to have_css '.pagination'
          end
        end

        it "has a selected number of pagination that is active" do
          within('.filterArea') do
            within(all('.active')[1]) do
              expect(page).to have_content '2'
            end
          end
        end
      end
    end

    # 'Pagination' with 9 or less products on potepan_categories_path
    context "when the number of products is 9 or less" do
      let!(:product) { [] }
      let!(:products) { [] }

      before { visit potepan_categories_path }

      it "displays all products" do
        within('.filterArea') do
          expect(all('.productBox').size).to eq Spree::Product.pluck(:id).size
        end
      end

      it "doesn't show 'pagination'" do
        within('.filterArea') do
          expect(page).not_to have_css '.pagination'
        end
      end
    end

    # 'FILTER BY SORT' on potepan_categories_path
    context "when sorted" do
      context "in the order of new arrival" do
        before { select("新着順", from: "sort_order") }

        it "has new arrival in the selected state" do
          expect(page).to have_select("sort_order", selected: "新着順")
        end
      end

      context "in the order of low price" do
        before { select("価格の安い順", from: "sort_order") }

        it "has low price in the selected state" do
          expect(page).to have_select("sort_order", selected: "価格の安い順")
        end
      end

      context "in the order of high price" do
        before { select("価格の高い順", from: "sort_order") }

        it "has high price in the selected state" do
          expect(page).to have_select("sort_order", selected: "価格の高い順")
        end
      end

      context "in the order of old arrival" do
        before { select("古い順", from: "sort_order") }

        it "has old arrival in the selected state" do
          expect(page).to have_select("sort_order", selected: "古い順")
        end
      end
    end

    # 'FILTER BY COLOR' on potepan_categories_path
    context "when using FILTER BY COLOR" do
      before do
        within(".panelColor") do
          click_link colors[0].presentation
        end
      end

      it "displays products of selected color" do
        expect(page).to have_content products[0].name
      end

      it "displays products as many as the selected color" do
        expect(all('.productBox').size).to eq [colors[0]].size
      end

      it "doesn't display products of color not selected" do
        expect(page).to have_no_content products[1].name
      end
    end

    # 'FILTER BY SIZE' on potepan_categories_path
    context "when using FILTER BY SIZE" do
      before do
        within(".panelSize") do
          click_link sizes[0].presentation
        end
      end

      it "displays products of selected size" do
        expect(page).to have_content products[0].name
      end

      it "displays products as many as the selected size" do
        expect(all('.productBox').size).to eq [sizes[0]].size
      end

      it "doesn't display products of color not size" do
        expect(page).to have_no_content products[1].name
      end
    end

    # 'SEARCH' on potepan_categories_path
    context "when using SEARCH" do
      context "with the search word included in any of the values in the name column of Spree::Product" do
        before do
          fill_in 'search_word', with: 'product0'
          click_button '検索'
        end

        it "displays products that contains the search word" do
          within('.filterArea') do
            expect(page).to have_content products[0].name
            expect(all('.productBox').size).to eq 1
          end
        end

        it "doesn't display products that isn't contain the search word" do
          within('.filterArea') do
            expect(page).not_to have_content products[1].name
          end
        end
      end

      context "with the search word included in any of the values in the description column of Spree::Product" do
        before do
          fill_in 'search_word', with: 'Sed'
          click_button '検索'
        end

        it "displays products that contains the search word" do
          within('.filterArea') do
            expect(page).to have_content products[1].name
            expect(all('.productBox').size).to eq 3
          end
        end

        it "doesn't display products that isn't contain the search word" do
          within('.filterArea') do
            expect(page).not_to have_content products[0].name
          end
        end
      end

      context "with the search word of 30 characters or less included in any of the values in the name or description column of Spree::Product" do
        let!(:variants_for_search) do
          [
            create(:variant, option_values: [colors[0]]),
            create(:variant, option_values: [sizes[0]]),
          ]
        end
        let!(:product_for_search) do
          create(
            :product,
            name: 'product_for_search',
            description: 'Sed ut perspiciatis unde omnis',
            price: 30.00,
            available_on: 1.day.ago,
            taxons: [taxons[1]],
            variants: [variants_for_search[0], variants_for_search[1]]
          )
        end
        let(:search_word) { 'Sed' }

        before do
          fill_in 'search_word', with: search_word
          click_button '検索'
        end

        it "displays search word in page title" do
          expect(page).to have_title search_word
          within('.page-title') do
            expect(page).to have_link search_word, href: "#{potepan_categories_path}?search_word=#{search_word}&sort_order=NEW_ARRIVAL"
          end
        end

        it "displays number of each color that products caught in search have in the side bar" do
          within(".panelColor") do
            expect(page).to have_content "(#{variants.select { |v| v.option_values == [colors[1]] }.size})"
            expect(page).to have_content "(#{variants_for_search.select { |v| v.option_values == [colors[0]] }.size})"
          end
        end

        it "displays number of each size that products caught in search have in the side bar" do
          within(".panelSize") do
            expect(page).to have_content "(#{variants.select { |v| v.option_values == [sizes[1]] }.size})"
            expect(page).to have_content "(#{variants_for_search.select { |v| v.option_values == [sizes[0]] }.size})"
          end
        end

        context "if using FILTER_BY_SORT" do
          before { select("価格の高い順", from: "sort_order") }

          it "has low price in the selected state" do
            expect(page).to have_select("sort_order", selected: "価格の高い順")
          end

          it "displays only products caught in search" do
            within('.filterArea') do
              expect(all('.productBox').size).to eq products[1..3].size + [product_for_search].size
            end
          end

          it "sorts products caught in search in the specified order" do
            within(all('.productBox').first) do
              expect(page).to have_content products[3].name
            end
          end
        end

        context "if using FILTER_BY_COLOR" do
          before do
            within(".panelColor") do
              click_link colors[1].presentation
            end
          end

          it "displays products caught in search and that match the selected color" do
            expect(page).to have_content products[1].name
            expect(all('.productBox').size).to eq products[1..3].size
          end

          it "doesn't display products caught in search and that don't match selected color" do
            expect(page).not_to have_content products[0].name
            expect(page).not_to have_content product_for_search.name
          end
        end

        context "if using FILTER_BY_SIZE" do
          before do
            within(".panelSize") do
              click_link sizes[1].presentation
            end
          end

          it "displays products caught in search and that match the selected size" do
            expect(page).to have_content products[1].name
            expect(all('.productBox').size).to eq products[1..3].size
          end

          it "doesn't display products caught in search and that don't match selected size" do
            expect(page).not_to have_content products[0].name
            expect(page).not_to have_content product_for_search.name
          end
        end
      end

      context "with the search word of 31 characters or more in any of the values in the name or description column of Spree::Product" do
        let(:default_search_word) { 'Products searched' }
        let(:search_word) { 'abcdefghijklmnopqrstuvwxyz12345' }

        before do
          fill_in 'search_word', with: search_word
          click_button '検索'
        end

        it "displays 'Products searched' in page title" do
          expect(page).to have_title default_search_word
          within('.page-title') do
            expect(page).to have_link default_search_word, href: "#{potepan_categories_path}?search_word=#{search_word}&sort_order=NEW_ARRIVAL"
          end
        end
      end
    end

    # jsテスト不可 →  selenium_chrome_headlessでのfalse解決不可のため
    # context "When using switch display" do
    #   it "sample", js: true do
    #     click_button 'Grid'
    #   end
    # end
  end

  # ------ on potepan_category_path ------
  context "on potepan_category_path" do
    context "with a valid path" do
      before { visit potepan_category_path(id: taxons[1].id) }

      it "has a title taxons[1].name" do
        expect(page).to have_title taxons[1].name
      end

      it "has search form and button" do
        within('.searchBox') do
          expect(page).to have_css '.search-form'
          expect(page).to have_css '.input-group-addon'
        end
      end

      it "displays HOME/SHOP/ link in breadcrumbs" do
        within('.pageHeader') do
          expect(page).to have_link 'HOME', href: potepan_path
          expect(page).to have_link 'SHOP', href: "#{potepan_categories_path}?sort_order=NEW_ARRIVAL"
        end
      end

      it "has a link to potepan_product_path(product.id)" do
        expect(page).to have_link products[0].display_price.to_s, href: potepan_product_path(products[0].id)
        expect(page).to have_link 'product_image', href: potepan_product_path(products[0].id)
      end

      it "has taxonomy and taxon values" do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content "#{taxons[0].name} (#{taxons[0].products.size})"
        expect(page).to have_content "#{taxons[1].name} (#{taxons[1].products.size})"
      end

      it "has links of color names" do
        expect(page).to have_link colors[0].presentation, href: "#{potepan_category_path(taxons[1].id)}?color_name=#{colors[0].name}&sort_order=NEW_ARRIVAL"
        expect(page).to have_link colors[1].presentation, href: "#{potepan_category_path(taxons[1].id)}?color_name=#{colors[1].name}&sort_order=NEW_ARRIVAL"
      end

      it "has number of each color" do
        within(".panelColor") do
          expect(page).to have_content "(#{variants.select { |v| v.option_values == [colors[0]] }.size})"
          expect(page).to have_content "(#{variants.select { |v| v.option_values == [colors[1]] }.size})"
        end
      end

      it "has links of size presentations" do
        expect(page).to have_link sizes[0].presentation, href: "#{potepan_category_path(taxons[1].id)}?size_name=#{sizes[0].name}&sort_order=NEW_ARRIVAL"
        expect(page).to have_link sizes[1].presentation, href: "#{potepan_category_path(taxons[1].id)}?size_name=#{sizes[1].name}&sort_order=NEW_ARRIVAL"
      end

      it "has number of each size" do
        within(".panelColor") do
          expect(page).to have_content "(#{variants.select { |v| v.option_values == [sizes[0]] }.size})"
          expect(page).to have_content "(#{variants.select { |v| v.option_values == [sizes[1]] }.size})"
        end
      end

      it "has buttons to switch product list display" do
        within(".displayStyle") do
          expect(page).to have_button 'Grid'
          expect(page).to have_button 'List'
        end
      end

      it "has 'Grid' button in active state" do
        within(all(".button_to").first) do
          expect(page).to have_css '.active'
        end
      end

      it "has 'List' button not in active state" do
        within(all(".button_to")[1]) do
          expect(page).not_to have_css '.active'
        end
      end

      it "has a product list display of 'Grid'" do
        within(".filterArea") do
          expect(page).not_to have_css '.productListSingle'
          expect(page).to have_css '.productBox'
          expect(page).not_to have_css '.media'
        end
      end

      context "when name of variables is selected" do
        it "links to potepan_category_path(taxons[0].id)" do
          click_link taxons[0].name
          expect(current_path).to eq potepan_category_path(taxons[0].id)
        end

        it "links to potepan_product_path(products[0].id)" do
          click_link products[0].name
          expect(current_path).to eq potepan_product_path(products[0].id)
        end
      end

      # 'Pagination' with 10 or more products on potepan_category_path
      context "when the number of products is 10 or more" do
        let!(:products_of_1_day_ago) do
          create_list(:product, 6, available_on: 1.day.ago, taxons: [taxons[1]])
        end

        before { visit potepan_category_path(id: taxons[1].id) }

        context "if pagination is in dafault state" do
          it "displays the first product" do
            within('.filterArea') do
              expect(page).to have_content products[3].name
            end
          end

          it "displays the 9th product" do
            within('.filterArea') do
              expect(page).to have_content products[1].name
            end
          end

          it "displays 9 products" do
            within('.filterArea') do
              expect(all('.productBox').size).to eq 9
            end
          end

          it "shows 'pagination'" do
            within('.filterArea') do
              expect(page).to have_css '.pagination'
            end
          end

          it "has a 'pagination 1' that is active" do
            within('.filterArea') do
              within(all('.active')[1]) do
                expect(page).to have_content '1'
              end
            end
          end
        end

        context "if pagination 2 is selected" do
          before do
            within('.pagination') do
              click_link '2'
            end
          end

          it "displays products after the 9th" do
            within('.filterArea') do
              expect(page).to have_content products[0].name
            end
          end

          it "displays 9 products or less" do
            within('.filterArea') do
              expect(all('.productBox').size).to be <= 9
            end
          end

          it "shows 'pagination'" do
            within('.filterArea') do
              expect(page).to have_css '.pagination'
            end
          end

          it "has a selected number of pagination that is active" do
            within('.filterArea') do
              within(all('.active')[1]) do
                expect(page).to have_content '2'
              end
            end
          end
        end
      end

      # 'Pagination' with 9 or less products on potepan_category_path
      context "when the number of products is 9 or less" do
        it "displays all products" do
          within('.filterArea') do
            expect(all('.productBox').size).to eq products.pluck(:id).size
          end
        end

        it "doesn't show 'pagination'" do
          within('.filterArea') do
            expect(page).not_to have_css '.pagination'
          end
        end
      end

      # 'FILTER BY SORT' on potepan_category_path
      context "when sorted" do
        context "in the order of new arrival" do
          before { select("新着順", from: "sort_order") }

          it "has new arrival in the selected state" do
            expect(page).to have_select("sort_order", selected: "新着順")
          end
        end

        context "in the order of low price" do
          before { select("価格の安い順", from: "sort_order") }

          it "has low price in the selected state" do
            expect(page).to have_select("sort_order", selected: "価格の安い順")
          end
        end

        context "in the order of high price" do
          before { select("価格の高い順", from: "sort_order") }

          it "has high price in the selected state" do
            expect(page).to have_select("sort_order", selected: "価格の高い順")
          end
        end

        context "in the order of old arrival" do
          before { select("古い順", from: "sort_order") }

          it "has old arrival in the selected state" do
            expect(page).to have_select("sort_order", selected: "古い順")
          end
        end
      end

      # 'FILTER BY COLOR' on potepan_category_path
      context "when using FILTER BY COLOR" do
        before do
          within(".panelColor") do
            click_link colors[0].presentation
          end
        end

        it "displays products of selected color" do
          expect(page).to have_content products[0].name
        end

        it "displays products as many as the selected color" do
          expect(all('.productBox').size).to eq [colors[0]].size
        end

        it "doesn't display products of color not selected" do
          expect(page).to have_no_content products[1].name
        end
      end

      # 'FILTER BY SIZE' on potepan_category_path
      context "when using FILTER BY SIZE" do
        before do
          within(".panelSize") do
            click_link sizes[0].presentation
          end
        end

        it "displays products of selected size" do
          expect(page).to have_content products[0].name
        end

        it "displays products as many as the selected size" do
          expect(all('.productBox').size).to eq [sizes[0]].size
        end

        it "doesn't display products of color not size" do
          expect(page).to have_no_content products[1].name
        end
      end
    end

    context "with a invalid path" do
      it "can't displays categories/show" do
        visit potepan_category_path(0)
        expect(current_path).to eq potepan_path
        expect(page).to have_css "div.alert"
      end
    end
  end
end
