require 'rails_helper'

RSpec.describe Spree::Taxonomy, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let!(:taxon1) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxon2) { create(:taxon, taxonomy: taxonomy, parent: taxon1) }

  it "returns taxons with depth 1" do
    expect(taxonomy.list_children).to eq [taxon1]
  end
end
