require 'rails_helper'

RSpec.describe Spree::Taxon, type: :model do
  let!(:taxonomy) { create(:taxonomy, id: 1) }
  let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxons) do
    [
      create(:taxon, name: 'Category1', taxonomy: taxonomy, parent: taxon),
      create(:taxon, name: 'Category2', taxonomy: taxonomy, parent: taxon),
      create(:taxon, name: 'Category3', taxonomy: taxonomy, parent: taxon),
      create(:taxon, name: 'Category4', taxonomy: taxonomy, parent: taxon),
    ]
  end
  let!(:products) do
    [
      create(:product, taxons: [taxons[0]]),
      create(:product, taxons: [taxons[0]]),
      create(:product, taxons: [taxons[0]]),
      create(:product, taxons: [taxons[0]]),
    ]
  end

  context "when using class method random_categories" do
    context "with number 3" do
      let(:popular_categories) { Spree::Taxon.random_categories(3) }

      it "returns 3 categories" do
        expect(popular_categories.size).to eq 3
      end

      it "returns categories of leaves" do
        expect(popular_categories).to include(taxons[0]).or include taxons[1]
      end

      it "doesn't return categories that are not leaves" do
        expect(popular_categories).not_to include taxon
      end
    end

    context "with number 4" do
      let(:popular_categories) { Spree::Taxon.random_categories(4) }

      it "returns 4 categories" do
        expect(popular_categories.size).to eq 4
      end

      it "returns categories of leaves" do
        expect(popular_categories).to match_array taxons
      end

      it "doesn't return categories that are not leaves" do
        expect(popular_categories).not_to include taxon
      end
    end
  end
end
