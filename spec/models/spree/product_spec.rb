require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:taxons) do
    [
      create(:taxon, taxonomy: taxonomy, parent: taxon),
      create(:taxon, taxonomy: taxonomy, parent: taxon),
    ]
  end
  let!(:option_types) do
    [
      create(:option_type, presentation: 'Color'),
      create(:option_type, presentation: 'Size'),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: 'Red', presentation: 'Red', option_type: option_types[0]),
      create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_types[0]),
    ]
  end
  let!(:sizes) do
    [
      create(:option_value, name: 'Medium', presentation: 'M', option_type: option_types[1]),
      create(:option_value, name: 'Large', presentation: 'L', option_type: option_types[1]),
    ]
  end
  let!(:variants) do
    [
      create_list(:variant, 2, option_values: [colors[0]]),
      create(:variant, option_values: [colors[1]]),
      create_list(:variant, 2, option_values: [sizes[0]]),
      create(:variant, option_values: [sizes[1]]),
    ]
  end
  let!(:products) do
    [
      create(
        :product,
        name: 'products[0]',
        price: 10.00,
        available_on: 2.years.ago,
        taxons: [taxons[0]],
        variants: [variants[0][0], variants[2][0]]
      ),
      create(
        :product,
        name: 'products[1]',
        price: 30.00,
        available_on: 1.years.ago,
        taxons: [taxons[1]],
        variants: [variants[0][1], variants[2][1]]
      ),
      create_list(
        :product,
        7,
        name: 'products[2]',
        price: 20.00,
        available_on: 1.days.ago,
        taxons: [taxons[0]],
        variants: [variants[1], variants[3]]
      ),
    ]
  end

  context "when using scope filter_by_sort" do
    let!(:latest_product) do
      create(
        :product,
        price: 20.00,
        available_on: Time.current,
        taxons: [taxons[0]]
      )
    end

    context "with NEW_ARRIVAL" do
      it "returns products in the order of new arrival" do
        expect(Spree::Product.filter_by_sort('NEW_ARRIVAL').first).to eq latest_product
      end
    end

    context "with LOW_PRICE" do
      it "returns products in the order of low price" do
        expect(Spree::Product.filter_by_sort('LOW_PRICE').first).to eq products[0]
      end
    end

    context "with HIGH_PRICE" do
      it "returns products in the order of high price" do
        expect(Spree::Product.filter_by_sort('HIGH_PRICE').first).to eq products[1]
      end
    end

    context "with OLD_ARRIVAL" do
      it "returns products in the order of old arrival" do
        expect(Spree::Product.filter_by_sort('OLD_ARRIVAL').first).to eq products[0]
      end
    end
  end

  context "when using method related_products" do
    context "with max_number 4" do
      it "returns related products in the same category" do
        expect(products[0].related_products(max_number: 4).map(&:name)).to match_array(products[2].take(4).map(&:name))
      end

      it "doesn't return the product itself" do
        expect(products[0].related_products(max_number: 4)).not_to include products[0]
      end

      it "doesn't return different category products" do
        expect(products[0].related_products(max_number: 4)).not_to include products[1]
      end

      it "returns up to 4 related products" do
        expect(products[0].related_products(max_number: 4).size).to eq(4)
      end
    end

    context "with max_number 6" do
      it "deosn't return the product itself" do
        expect(products[0].related_products(max_number: 6)).not_to include products[0]
      end

      it "doesn't return different category products" do
        expect(products[0].related_products(max_number: 6)).not_to include products[1]
      end

      it "returns up to 6 related products" do
        expect(products[0].related_products(max_number: 6).size).to eq(6)
      end
    end
  end

  context "when using method referrer_taxon_id_and_name" do
    let!(:product_with_2_taxons) { create(:product, taxons: [taxons[0], taxons[1]]) }
    let(:regexp) { /potepan\/categories\/\d+/ }

    context "with request_referrer: potepan_category_path" do
      let!(:request_referrer) { "/potepan/categories/#{taxons[1].id}" }

      it "returns referrer taxon id and name" do
        expect(product_with_2_taxons.referrer_taxon_id_and_name(regexp_of_potepan_category_path: regexp, request_referrer: request_referrer)).to eq [taxons[1].id, taxons[1].name]
      end
    end

    context "that doesn't have request_referrer: potepan_category_path" do
      let!(:request_referrer) { '/potepan/categories' }

      it "returns the first taxon id and name" do
        expect(product_with_2_taxons.referrer_taxon_id_and_name(regexp_of_potepan_category_path: regexp, request_referrer: request_referrer)).to eq [taxons[0].id, taxons[0].name]
      end
    end
  end

  context "when using method self.new_products" do
    context "with number 8" do
      it "returns 8 new products" do
        expect(Spree::Product.new_products(number: 8).size).to eq(8)
      end

      it "doesn't return 9th product in the order of new arrival" do
        expect(Spree::Product.new_products(number: 8)).not_to include products[0]
      end
    end

    context "with number 7" do
      it "returns 7 new products" do
        expect(Spree::Product.new_products(number: 7).size).to eq(7)
      end

      it "doesn't return 8th product in the order of new arrival" do
        expect(Spree::Product.new_products(number: 7)).not_to include products[1]
      end
    end
  end

  context "when using method self.filter" do
    let!(:variants_for_filter) do
      [
        create(:variant, option_values: [colors[0]]),
        create(:variant, option_values: [sizes[0]]),
      ]
    end
    let!(:product_for_filter) do
      create(
        :product,
        name: 'product_for_filter',
        price: 20.00,
        available_on: 1.years.ago,
        taxons: [taxons[0]],
        variants: [variants_for_filter[0], variants_for_filter[1]]
      )
    end
    let!(:sort_values) do
      [
        ["新着順", "NEW_ARRIVAL"],
        ["価格の安い順", "LOW_PRICE"],
        ["価格の高い順", "HIGH_PRICE"],
        ["古い順", "OLD_ARRIVAL"],
      ]
    end

    context "with taxon present" do
      context "and sort_order present" do
        context "and color name present" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], sort_order: sort_values[2][1], color_name: colors[0].name, sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon and color" do
            expect(products_filtered).to match_array [products[0], product_for_filter]
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.first).to eq product_for_filter
          end
        end

        context "and size name present" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], sort_order: sort_values[2][1], size_name: sizes[0].name, sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon and size" do
            expect(products_filtered).to match_array [products[0], product_for_filter]
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.first).to eq product_for_filter
          end
        end

        context "and color name nil and size name nil" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], sort_order: sort_values[2][1], sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon" do
            expect(products_filtered).to match_array [products[0], products[2], product_for_filter].flatten
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.reverse.first).to eq products[0]
          end
        end
      end

      context "and sort_order nil" do
        context "and color name present" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], color_name: colors[0].name, sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon and color" do
            expect(products_filtered).to match_array [products[0], product_for_filter]
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.first).to eq product_for_filter
          end
        end

        context "and size name present" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], size_name: sizes[0].name, sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon and size" do
            expect(products_filtered).to match_array [products[0], product_for_filter]
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.first).to eq product_for_filter
          end
        end

        context "and color name nil and size name nil" do
          let(:products_filtered) do
            Spree::Product.filter(taxon: taxons[0], sort_value: sort_values[0][1])
          end

          it "returns products that have the selected taxon" do
            expect(products_filtered).to match_array [products[0], products[2], product_for_filter].flatten
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.reverse.first).to eq products[0]
          end
        end
      end
    end

    context "with taxon nil" do
      context "and sort_order present" do
        context "and color name present" do
          let(:products_filtered) do
            Spree::Product.filter(sort_order: sort_values[2][1], color_name: colors[0].name, sort_value: sort_values[0][1])
          end

          it "returns only products that have the selected color" do
            expect(products_filtered).to match_array [products[0], products[1], product_for_filter]
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.first).to eq products[1]
          end
        end

        context "and size name present" do
          let(:products_filtered) do
            Spree::Product.filter(sort_order: sort_values[2][1], size_name: sizes[0].name, sort_value: sort_values[0][1])
          end

          it "returns only products that have the selected size" do
            expect(products_filtered).to match_array [products[0], products[1], product_for_filter]
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.first).to eq products[1]
          end
        end

        context "and color name nil and size name nil" do
          let(:products_filtered) do
            Spree::Product.filter(sort_order: sort_values[2][1], sort_value: sort_values[0][1])
          end

          it "returns all products" do
            expect(products_filtered.pluck(:id)).to match_array Spree::Product.pluck(:id)
          end

          it "sorts the products in the specified order" do
            expect(products_filtered.first).to eq products[1]
          end
        end
      end

      context "and sort_order nil" do
        context "and color name present" do
          let(:products_filtered) do
            Spree::Product.filter(color_name: colors[0].name, sort_value: sort_values[0][1])
          end

          it "returns only products that have the selected color" do
            expect(products_filtered).to match_array [products[0], products[1], product_for_filter]
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.reverse.first).to eq products[0]
          end
        end

        context "and size name present" do
          let(:products_filtered) do
            Spree::Product.filter(size_name: sizes[0].name, sort_value: sort_values[0][1])
          end

          it "returns only products that have the selected taxon and size" do
            expect(products_filtered).to match_array [products[0], products[1], product_for_filter]
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.reverse.first).to eq products[0]
          end
        end

        context "and color name nil and size name nil" do
          let(:products_filtered) do
            Spree::Product.filter(sort_value: sort_values[0][1])
          end

          it "returns all products" do
            expect(products_filtered.pluck(:id)).to match_array Spree::Product.pluck(:id)
          end

          it "sorts the products in the NEW_ARRIVAL order" do
            expect(products_filtered.pluck(:id).reverse.first).to eq products[0].id
          end
        end
      end
    end
  end

  context "when using method self.filter_by_option_values" do
    context "with colors" do
      context "if taxon is present" do
        it "returns products of selected color in current taxon" do
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: colors[0].name).uniq).to eq [products[0]]
          expect(Spree::Product.filter_by_option_values(taxon: taxons[1], option_value_name: colors[0].name).uniq).to eq [products[1]]
        end

        it "doesn't return products of color not selected in current taxon" do
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: colors[0].name).uniq).not_to include products[2]
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: colors[0].name).uniq).not_to include products[1]
        end
      end

      context "if taxon is blank" do
        it "returns products of selected color" do
          expect(Spree::Product.filter_by_option_values(taxon: nil, option_value_name: colors[0].name).uniq).to eq [products[0], products[1]]
        end

        it "doesn't return products of color not selected" do
          expect(Spree::Product.filter_by_option_values(taxon: nil, option_value_name: colors[0].name).uniq).not_to include products[3]
        end
      end
    end

    context "with sizes" do
      context "if taxon is present" do
        it "returns products of selected size in current taxon" do
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: sizes[0].name).uniq).to eq [products[0]]
          expect(Spree::Product.filter_by_option_values(taxon: taxons[1], option_value_name: sizes[0].name).uniq).to eq [products[1]]
        end

        it "doesn't return products of size not selected in current taxon" do
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: sizes[0].name).uniq).not_to include products[2]
          expect(Spree::Product.filter_by_option_values(taxon: taxons[0], option_value_name: sizes[0].name).uniq).not_to include products[1]
        end
      end

      context "if taxon is blank" do
        it "returns products of selected size" do
          expect(Spree::Product.filter_by_option_values(taxon: nil, option_value_name: sizes[0].name).uniq).to eq [products[0], products[1]]
        end

        it "doesn't return products of size not selected" do
          expect(Spree::Product.filter_by_option_values(taxon: nil, option_value_name: sizes[0].name).uniq).not_to include products[2]
        end
      end
    end
  end
end
