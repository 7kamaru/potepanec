require 'rails_helper'

RSpec.describe Spree::Order, type: :model do
  context "when using method 'item_or_items'" do
    context "with one line item" do
      let(:order) do
        create(:order_with_line_items, line_items_count: 1)
      end

      it "returns 'item'" do
        expect(order.item_or_items).to eq 'item'
      end
    end

    context "when the number of line items is other than one" do
      let(:order) do
        create(:order_with_line_items, line_items_count: 2)
      end

      it "returns 'items'" do
        expect(order.item_or_items).to eq 'items'
      end
    end
  end

  context "when using method 'consumption_tax_total'" do
    let(:order) { create(:order, total: 3500) }

    context "with consumption_tax_rate: 0.08" do
      let(:rate_8) { 0.08 }
      let(:consumption_tax_total) { "280" }

      it "returns the consumption tax of the order in rate 0.08" do
        expect(order.consumption_tax_total(consumption_tax_rate: rate_8)).to eq consumption_tax_total
      end
    end

    context "with consumption_tax_rate: 0.1" do
      let(:rate_10) { 0.1 }
      let(:consumption_tax_total) { "350" }

      it "returns the consumption tax of the order in rate 0.1" do
        expect(order.consumption_tax_total(consumption_tax_rate: rate_10)).to eq consumption_tax_total
      end
    end
  end

  context "when using method 'shipment_cost_total'" do
    let(:shipping_cost_per_1) { 200 }
    let(:shipping_cost) { 650 }

    context "with 1 line item" do
      let(:order) { create(:order, item_count: 1) }

      it "returns shipping_cost_per_1" do
        expect(order.shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)).to eq shipping_cost_per_1
      end
    end

    context "with 2 to 4 line items" do
      let(:orders) do
        [
          create(:order, item_count: 2),
          create(:order, item_count: 4),
        ]
      end

      it "returns shipping_cost" do
        expect(orders[0].shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)).to eq shipping_cost
        expect(orders[1].shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)).to eq shipping_cost
      end
    end

    context "with line items other than 1 to 4" do
      let(:orders) do
        [
          create(:order, item_count: 0),
          create(:order, item_count: 5),
        ]
      end

      it "returns 0" do
        expect(orders[0].shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)).to eq 0
        expect(orders[1].shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)).to eq 0
      end
    end
  end

  context "when using method 'order_price_total'" do
    let(:order)  { create(:order, total: 3500, item_count: 1) }
    let(:rate_8) { 0.08 }
    let(:shipping_cost_per_1) { 200 }
    let(:shipping_cost) { 650 }
    let(:consumption_tax_total) do
      order.consumption_tax_total(consumption_tax_rate: rate_8)
    end
    let(:shipment_cost_total) do
      order.shipment_cost_total(shipping_cost_per_1: shipping_cost_per_1, shipping_cost: shipping_cost)
    end
    let(:order_price_total) { "3,980" }

    it "returns the total amount of the order" do
      expect(order.order_price_total(consumption_tax_total: consumption_tax_total, shipment_cost_total: shipment_cost_total)).to eq order_price_total
    end
  end
end
