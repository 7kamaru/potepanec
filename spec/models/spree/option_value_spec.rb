require 'rails_helper'

RSpec.describe Spree::OptionValue, type: :model do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:taxons) do
    [
      create(:taxon, taxonomy: taxonomy, parent: taxon),
      create(:taxon, taxonomy: taxonomy, parent: taxon),
    ]
  end
  let!(:option_types) do
    [
      create(:option_type, presentation: 'Color'),
      create(:option_type, presentation: 'Size'),
    ]
  end
  let!(:colors) do
    [
      create(:option_value, name: 'Red', presentation: 'Red', option_type: option_types[0]),
      create(:option_value, name: 'Blue', presentation: 'Blue', option_type: option_types[0]),
    ]
  end
  let!(:sizes) do
    [
      create(:option_value, name: 'Medium', presentation: 'M', option_type: option_types[1]),
      create(:option_value, name: 'Large', presentation: 'L', option_type: option_types[1]),
    ]
  end
  let!(:variants) do
    [
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [colors[1]]),
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [colors[0]]),
      create(:variant, option_values: [sizes[0]]),
      create(:variant, option_values: [sizes[1]]),
      create(:variant, option_values: [sizes[0]]),
      create(:variant, option_values: [sizes[0]]),
    ]
  end
  let!(:products) do
    [
      create(:product, taxons: [taxons[0]], variants: [variants[0], variants[4]]),
      create(:product, taxons: [taxons[0]], variants: [variants[1], variants[5]]),
      create(:product, taxons: [taxons[0]], variants: [variants[2], variants[6]]),
      create(:product, taxons: [taxons[1]], variants: [variants[3], variants[7]]),
    ]
  end

  context "when using method counts_products_in_current_taxon" do
    let(:ids_of_products_in_current_taxon) { taxons[0].products.pluck(:id) }

    context "with colors[0]" do
      it "returns number of products in specified color and taxon" do
        expect(colors[0].counts_products_in_current_taxon(ids_of_products_in_current_taxon)).to eq [products[0], products[2]].size
      end
    end

    context "with colors[1]" do
      it "returns number of products in specified color and taxon" do
        expect(colors[1].counts_products_in_current_taxon(ids_of_products_in_current_taxon)).to eq [products[1]].size
      end
    end

    context "with sizes[0]" do
      it "returns number of products in specified color and taxon" do
        expect(colors[0].counts_products_in_current_taxon(ids_of_products_in_current_taxon)).to eq [products[0], products[2]].size
      end
    end

    context "with sizes[1]" do
      it "returns number of products in specified color and taxon" do
        expect(colors[1].counts_products_in_current_taxon(ids_of_products_in_current_taxon)).to eq [products[1]].size
      end
    end
  end
end
