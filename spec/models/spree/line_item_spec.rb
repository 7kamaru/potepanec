require 'rails_helper'

RSpec.describe Spree::LineItem, type: :model do
  context "when using method 'quantity_updated'" do
    let(:line_item) { create(:line_item, quantity: 2) }

    context "with calculation: 'plus'" do
      let(:plus) { 'plus' }

      it "returns the number of the line item's quantity plus 1" do
        expect(line_item.quantity_updated(calculation: plus)).to eq line_item.quantity + 1
      end
    end

    context "with calculation: 'minus'" do
      let(:minus) { 'minus' }

      it "returns the number of the line item's quantity minus 1" do
        expect(line_item.quantity_updated(calculation: minus)).to eq line_item.quantity - 1
      end
    end
  end
end
