Spree::Product.class_eval do
  paginates_per 9
  scope :includes_display_price_and_image, -> { includes({ master: [:default_price, :images] }, :variant_images) }
  scope :new_arrival_order, -> { except(:order).order(available_on: :desc) }
  scope :low_price_order, -> { except(:order).ascend_by_master_price }
  scope :high_price_order, -> { except(:order).descend_by_master_price }
  scope :old_arrival_order, -> { except(:order).order(available_on: :asc) }
  scope :filter_by_sort, -> (sort_order) do
    case sort_order
    when 'NEW_ARRIVAL'
      new_arrival_order
    when 'LOW_PRICE'
      low_price_order
    when 'HIGH_PRICE'
      high_price_order
    when 'OLD_ARRIVAL'
      old_arrival_order
    end
  end

  def related_products(max_number: 4)
    Spree::Product.includes_display_price_and_image.find(Spree::Product.in_taxons(taxons).where.not(id: id).distinct.pluck(:id).shuffle[0..(max_number - 1)])
  end

  def referrer_taxon_id_and_name(regexp_of_potepan_category_path:, request_referrer:)
    if request_referrer&.match(regexp_of_potepan_category_path)
      referrer_url = request_referrer
      taxon_id = /potepan\/categories\/(\d+)/.match(referrer_url)
      taxons.pluck(:id, :name).select { |taxon| taxon[0] == taxon_id[1].to_i }.flatten
    else
      taxons.pluck(:id, :name).first
    end
  end

  def self.new_products(number: 8)
    includes_display_price_and_image.new_arrival_order.limit(number)
  end

  def self.filter(taxon: nil, sort_order: nil, color_name: nil, size_name: nil, sort_value: nil)
    if taxon && sort_order && color_name
      filter_by_option_values(taxon: taxon, option_value_name: color_name).filter_by_sort(sort_order).uniq
    elsif taxon && sort_order && size_name
      filter_by_option_values(taxon: taxon, option_value_name: size_name).filter_by_sort(sort_order).uniq
    elsif taxon && sort_order
      in_taxons(taxon).includes_display_price_and_image.filter_by_sort(sort_order).uniq
    elsif taxon && color_name
      filter_by_option_values(taxon: taxon, option_value_name: color_name).filter_by_sort(sort_value).uniq
    elsif taxon && size_name
      filter_by_option_values(taxon: taxon, option_value_name: size_name).filter_by_sort(sort_value).uniq
    elsif taxon
      in_taxons(taxon).includes_display_price_and_image.filter_by_sort(sort_value).uniq
    elsif sort_order && color_name
      filter_by_option_values(taxon: nil, option_value_name: color_name).filter_by_sort(sort_order).uniq
    elsif sort_order && size_name
      filter_by_option_values(taxon: nil, option_value_name: size_name).filter_by_sort(sort_order).uniq
    elsif sort_order
      includes_display_price_and_image.filter_by_sort(sort_order).uniq
    elsif color_name
      filter_by_option_values(taxon: nil, option_value_name: color_name).filter_by_sort(sort_value).uniq
    elsif size_name
      filter_by_option_values(taxon: nil, option_value_name: size_name).filter_by_sort(sort_value).uniq
    else
      includes_display_price_and_image.filter_by_sort(sort_value).uniq
    end
  end

  # Distinct不使用のためproductsが重複する。
  # 不使用 -> MySQLでorderと両立不可のため。
  # 使用時は呼び出し側メソッド末尾に要uniq使用。
  def self.filter_by_option_values(taxon:, option_value_name:)
    if taxon.present?
      in_taxons(taxon).includes_display_price_and_image.joins(variants: :option_values).where(spree_option_values: { name: option_value_name })
    else
      includes_display_price_and_image.joins(variants: :option_values).where(spree_option_values: { name: option_value_name })
    end
  end
end
