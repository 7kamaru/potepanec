Spree::Order.class_eval do
  def item_or_items
    if item_count == 1
      'item'
    else
      'item'.pluralize
    end
  end

  def consumption_tax_total(consumption_tax_rate: 0.08)
    consumption_tax_total = total * consumption_tax_rate
    consumption_tax_total.floor.to_s(:delimited, delimiter: ',')
  end

  def shipment_cost_total(shipping_cost_per_1: 200, shipping_cost: 650)
    if item_count == 1
      shipping_cost_per_1
    elsif item_count >= 2 && item_count < 5
      shipping_cost
    else
      0
    end
  end

  def order_price_total(consumption_tax_total:, shipment_cost_total:)
    order_price_total = total + consumption_tax_total.to_i + shipment_cost_total
    order_price_total.floor.to_s(:delimited, delimiter: ',')
  end
end
