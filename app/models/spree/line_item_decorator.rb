Spree::LineItem.class_eval do
  def quantity_updated(calculation:)
    if calculation == 'plus'
      quantity + 1
    elsif calculation == 'minus'
      quantity - 1
    end
  end
end
