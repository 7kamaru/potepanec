Spree::OptionValue.class_eval do
  def counts_products_in_current_taxon(ids_of_products_in_current_taxon)
    variants.map(&:product).uniq.pluck(:id).select { |ids| ids_of_products_in_current_taxon.include?(ids) }.size
  end
end
