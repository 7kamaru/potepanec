Spree::OptionType.class_eval do
  scope :colors, -> { includes(option_values: { variants: :product }).find_by(presentation: 'Color').option_values }
  scope :sizes, -> { includes(option_values: { variants: :product }).find_by(presentation: 'Size').option_values }
end
