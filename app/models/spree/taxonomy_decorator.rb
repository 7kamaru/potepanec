Spree::Taxonomy.class_eval do
  def list_children(depth: 1)
    taxons.select { |taxon| taxon.depth == depth }
  end
end
