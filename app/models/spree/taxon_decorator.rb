Spree::Taxon.class_eval do
  def self.random_categories(number)
    ids_of_popular_categories = Spree::Taxon.leaves.where(taxonomy_id: 1).pluck(:id).shuffle![0..(number - 1)]
    includes(products: [{ master: :images }, :variant_images]).find(ids_of_popular_categories)
  end
end
