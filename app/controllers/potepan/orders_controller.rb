class Potepan::OrdersController < ApplicationController
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Store
  CONSUMPTION_TAX_RATE = 0.08
  SHIPPING_COST_PER_1 = 200
  SHIPPING_COST = 650

  def index
    @order = current_order || Spree::Order.incomplete.find_or_initialize_by(guest_token: cookies.signed[:guest_token])
    @line_items = @order.line_items.includes(variant: [{ product: :variant_images }, :images])
    @item_or_items = @order.item_or_items
    @consumption_tax_total = @order.consumption_tax_total(consumption_tax_rate: CONSUMPTION_TAX_RATE)
    @shipment_cost_total = @order.shipment_cost_total(shipping_cost_per_1: SHIPPING_COST_PER_1, shipping_cost: SHIPPING_COST)
    @order_price_total = @order.order_price_total(consumption_tax_total: @consumption_tax_total, shipment_cost_total: @shipment_cost_total)
  end

  def create
    @order = Spree::Order.find_by(guest_token: cookies.signed[:guest_token]) || Spree::Order.create(guest_token: cookies.signed[:guest_token])

    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].to_i

    if !quantity.between?(1, 2_147_483_647)
      @order.errors.add(:base, t('spree.please_enter_reasonable_quantity'))
    end

    begin
      @line_item = Spree::Order.includes(line_items: :adjustments).find(@order.id).contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => e
      @order.errors.add(:base, e.record.errors.full_messages.join(", "))
    end

    if @order.errors.any?
      flash[:danger] = @order.errors.full_messages.join(", ")
      redirect_back_or_default potepan_path
      return
    else
      redirect_to potepan_orders_path
    end
  end

  def update
    @order = Spree::Order.includes([{ variants: :product }, { line_items: [:tax_category, :adjustments] }]).find_by(number: params[:number])
    @line_item_id = order_params[:line_items_attributes][0][:id]
    @line_item = @order.line_items.find(@line_item_id)
    @line_item_quantity_updated = @line_item.quantity_updated(calculation: params[:calculation])
    @order.contents.update_cart({ line_items_attributes: [{ id: @line_item_id, quantity: @line_item_quantity_updated }] })
    @item_or_items = @order.item_or_items
    @consumption_tax_total = @order.consumption_tax_total(consumption_tax_rate: CONSUMPTION_TAX_RATE)
    @shipment_cost_total = @order.shipment_cost_total(shipping_cost_per_1: SHIPPING_COST_PER_1, shipping_cost: SHIPPING_COST)
    @order_price_total = @order.order_price_total(consumption_tax_total: @consumption_tax_total, shipment_cost_total: @shipment_cost_total)
  end

  def destroy
    @line_item = Spree::LineItem.find(params[:id])
    @order = Spree::Order.includes([line_items: :adjustments]).find(@line_item.order_id)
    @order.contents.remove_line_item(@line_item)
    @item_or_items = @order.item_or_items
    @consumption_tax_total = @order.consumption_tax_total(consumption_tax_rate: CONSUMPTION_TAX_RATE)
    @shipment_cost_total = @order.shipment_cost_total(shipping_cost_per_1: SHIPPING_COST_PER_1, shipping_cost: SHIPPING_COST)
    @order_price_total = @order.order_price_total(consumption_tax_total: @consumption_tax_total, shipment_cost_total: @shipment_cost_total)
  end

  private

  def order_params
    params.require(:order).permit(line_items_attributes: [:id])
  end
end
