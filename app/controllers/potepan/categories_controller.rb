class Potepan::CategoriesController < ApplicationController
  include CommonActions
  before_action :current_order_and_line_items # app/controllers/concerns/common_action.rb:8
  SORT_VALUES =
    [
      ["新着順", "NEW_ARRIVAL"],
      ["価格の安い順", "LOW_PRICE"],
      ["価格の高い順", "HIGH_PRICE"],
      ["古い順", "OLD_ARRIVAL"],
    ].freeze
  MAX_NUMBER_OF_SEARCH_WORD_FOR_DISPLAY = 30
  DEFAULT_SEARCH_WORD_FOR_DISPLAY = 'Products searched'.freeze

  def index
    @taxonomies = Spree::Taxonomy.includes(taxons: [{ children: :products }, :products])
    @colors = Spree::OptionType.colors
    @sizes = Spree::OptionType.sizes
    @sort_values = SORT_VALUES
    @selected_sort_value = filter_params[:sort_order] || SORT_VALUES[0][1]
    if params[:search_word] && params[:search_word].size <= MAX_NUMBER_OF_SEARCH_WORD_FOR_DISPLAY
      @search_word = params[:search_word]
    elsif params[:search_word]
      @search_word = params[:search_word]
      @default_search_word = DEFAULT_SEARCH_WORD_FOR_DISPLAY
    end
    if @search_word.present?
      products_searched = Spree::Product.like_any([:name, :description], [params[:search_word]])
      products =
        products_searched.filter(taxon: nil, sort_order: filter_params[:sort_order], color_name: filter_params[:color_name], size_name: filter_params[:size_name], sort_value: @sort_values[0][1])
      @ids_of_products_in_current_taxon = products_searched.pluck(:id)
    else
      products =
        Spree::Product.filter(taxon: nil, sort_order: filter_params[:sort_order], color_name: filter_params[:color_name], size_name: filter_params[:size_name], sort_value: @sort_values[0][1])
      @ids_of_products_in_current_taxon = Spree::Product.pluck(:id)
    end
    @products =
      if products.class == Array
        Kaminari.paginate_array(products).page(filter_params[:page])
      else
        products.page(filter_params[:page])
      end
    render action: :show
  end

  def show
    @taxonomies = Spree::Taxonomy.includes(taxons: [{ children: :products }, :products])
    @taxon = Spree::Taxon.find(params[:id])
    @colors = Spree::OptionType.colors
    @sizes = Spree::OptionType.sizes
    @sort_values = SORT_VALUES
    @selected_sort_value = filter_params[:sort_order] || SORT_VALUES[0][1]
    products = Spree::Product.filter(taxon: @taxon, sort_order: filter_params[:sort_order], color_name: filter_params[:color_name], size_name: filter_params[:size_name], sort_value: @sort_values[0][1])
    @products =
      if products.class == Array
        Kaminari.paginate_array(products).page(filter_params[:page])
      else
        products.page(filter_params[:page])
      end
    @ids_of_products_in_current_taxon = @taxon.products.pluck(:id)
  end

  def grid
    session.delete(:display_style) if session[:display_style].present?
  end

  def list
    session[:display_style] = 'list'
  end

  private

  def filter_params
    { sort_order: params[:sort_order], color_name: params[:color_name], size_name: params[:size_name], page: params[:page] }
  end

  def resource_not_found
    message = "お探しの商品は見つかりませんでした。"
    flash[:danger] = message
    redirect_to controller: 'products', action: 'index'
  end
end
