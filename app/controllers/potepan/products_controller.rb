class Potepan::ProductsController < ApplicationController
  include CommonActions
  before_action :current_order_and_line_items # app/controllers/concerns/common_action.rb:8
  NUMBER_OF_POPULAR_CATEGORIES = 3
  NUMBER_OF_NEW_PRODUCTS = 8
  MAX_NUMBER_OF_RELATED_PRODUCTS = 4
  REGEXP_OF_POTEPAN_CATEGORY_PATH = /potepan\/categories\/\d+/

  def index
    @popular_categories = Spree::Taxon.random_categories(NUMBER_OF_POPULAR_CATEGORIES)
    @display_products_in_popular_categories = @popular_categories.map { |taxon| taxon.products.first }
    @new_products = Spree::Product.new_products(number: NUMBER_OF_NEW_PRODUCTS)
  end

  def show
    @product = Spree::Product.find(params[:id])
    @images = @product.variant_images
    @related_products = @product.related_products(max_number: MAX_NUMBER_OF_RELATED_PRODUCTS)
    @taxon_id_and_name =
      @product.referrer_taxon_id_and_name(regexp_of_potepan_category_path: REGEXP_OF_POTEPAN_CATEGORY_PATH, request_referrer: request.referrer)
    arrange_referrer
  end

  private

  def arrange_referrer
    if request.referer&.match(REGEXP_OF_POTEPAN_CATEGORY_PATH)
      @referrer = request.referrer
    else
      @referrer = potepan_category_path(@product.taxons.ids.first)
    end
  end

  def resource_not_found
    message = "お探しの商品は見つかりませんでした。"
    flash[:danger] = message
    redirect_to controller: 'products', action: 'index'
  end
end
