module CommonActions
  extend ActiveSupport::Concern
  include Spree::Core::ControllerHelpers::Order
  include Spree::Core::ControllerHelpers::Auth
  include Spree::Core::ControllerHelpers::Store
  CONSUMPTION_TAX_RATE = 0.08
  SHIPPING_COST_PER_1 = 200
  SHIPPING_COST = 650

  def current_order_and_line_items
    @order = current_order || Spree::Order.incomplete.find_or_initialize_by(guest_token: cookies.signed[:guest_token])
    @line_items = @order.line_items.includes(variant: [{ product: :variant_images }, :images])
    @item_or_items = @order.item_or_items
    @consumption_tax_total = @order.consumption_tax_total(consumption_tax_rate: CONSUMPTION_TAX_RATE)
    @shipment_cost_total = @order.shipment_cost_total(shipping_cost_per_1: SHIPPING_COST_PER_1, shipping_cost: SHIPPING_COST)
    @order_price_total = @order.order_price_total(consumption_tax_total: @consumption_tax_total, shipment_cost_total: @shipment_cost_total)
  end
end
